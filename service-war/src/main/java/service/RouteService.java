package service;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.hibernate.cfg.Configuration;

import entities.Route;
import repositories.RouteRepository;

@WebService
public class RouteService {
	private RouteRepository routeFile;
	private List<Route> routes;
	
	public RouteService()
	{
		this.routeFile=new RouteRepository(new Configuration().configure().buildSessionFactory() );
		this.setRoutes();
	}
	
	private void setRoutes() {
		this.routes=(ArrayList<Route>) routeFile.findRoutes();
	}
	
	@WebMethod
	public Set<String> getDeclaredFields()
	{
		Set<String> fields=new HashSet<String>();
		
		Field[] routeCols=Route.class.getDeclaredFields();
		int nrCols=routeCols.length;
		for(int i=0;i<nrCols;i++)
			fields.add(routeCols[i].getName().substring(0, 1).toUpperCase()+routeCols[i].getName().substring(1));
		
		return fields;
	}

	@WebMethod
	public List<Route> getRoutes() {
		return new ArrayList<Route>(routes);
	}
	@WebMethod
	public List<Route> searchRoutes(int packageId)
	{
		List<Route> rez=new ArrayList<Route>();
		for(Route e:routes)
			if(e.getPackageId()==packageId)
				rez.add(e);
		
		return rez;
	}
	@WebMethod
	public Route getRoute(int Id)
	{
		Route rez=null;
		for(Route e:routes)
			if(e.getId()==Id)
				rez=e;
		
		return rez;
	}
	@WebMethod
	public Route deleteRoute(int id)
	{
		Route r=null;
		if((r=routeFile.deleteRouteId(id))!=null)
		{
			this.routes=this.routeFile.findRoutes();
			return r;
		}else return null;
	}
	@WebMethod
	public List<Route> deletePackageRoutes(int id)
	{
		List<Route> r=null;
		if((r=routeFile.deleteRoutePackage(id))!=null)
		{
			this.routes=this.routeFile.findRoutes();
			return r;
		}else return null;
	}
	@WebMethod
	public void updateRoute(Route Route)
	{
		routeFile.updateRoute(Route);
		this.routes =routeFile.findRoutes();
	}
	@WebMethod
	public void insertRoute(Route Route)
	{
		routeFile.addRoute(Route);
		this.routes =  routeFile.findRoutes();
	}
	
	@WebMethod
	public void populate()
	{
		try {
			routeFile.populate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setRoutes();
	}

}
