package service;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.hibernate.cfg.Configuration;

import entities.Package;
import entities.User;
import repositories.PackageRepository;
import repositories.UserRepository;

@WebService
public class PackageService{
	
	
	private List<Package> packs;
	private List<User> users;
	
	private PackageRepository packFile;
	private UserRepository userFile;
	

	public PackageService()
	{
		
		this.packFile=new PackageRepository(new Configuration().configure().buildSessionFactory());
		this.userFile=new UserRepository(new Configuration().configure().buildSessionFactory());
		
		this.setPackages();
	}
	
	@WebMethod
	public List<Package> getPackages() {
		return new ArrayList<Package>(packs);
	}
	
	@WebMethod
	public Set<String> getDeclaredFields()
	{
		Set<String> fields=new HashSet<String>();
		
		Field[] empCols=Package.class.getDeclaredFields();
		int nrCols=empCols.length;
		for(int i=0;i<nrCols;i++)
			fields.add(empCols[i].getName().substring(0, 1).toUpperCase()+empCols[i].getName().substring(1));
		
		return fields;
	}
	
	
	@WebMethod
	public String printPackages() {
		return packs.toString();
	}
	
	
	protected void setPackages(List<Package> emps) {
		this.packs = emps;
	}
	
	protected void setPackages() {
		this.packs =  packFile.findPackages();
		this.users = userFile.findUsers();
		
		for(Package f:packs)
		{
			for(User c:users)
			{
				if(f.getSenderId()==c.getId())
					f.setSender(c);
				if(f.getReceiverId()==c.getId())
					f.setReceiver(c);
				
			}
		}
		
	}
	
	protected void setUsers() {
		this.users = (ArrayList<User>) userFile.findUsers();
	}
	@WebMethod
	public List<Package> searchPackageName(String name)
	{
		List<Package> rez=new ArrayList<Package>();
		for(Package e:packs)
			if(e.getName().toLowerCase().contains(name.toLowerCase()))
				rez.add(e);
		
		return rez;
	}
	
	@WebMethod
	public List<Package> searchPackageSnd(int name)
	{
		List<Package> rez=new ArrayList<Package>();
		for(Package e:packs)
			if(e.getSenderId()==name)
				rez.add(e);
		
		return rez;
	}
	@WebMethod
	public List<Package> searchPackageId(int id)
	{
		List<Package> rez=new ArrayList<Package>();
		for(Package e:packs)
			if(e.getId()==id)
				rez.add(e);
		
		return rez;
	}

	@WebMethod
	public List<Package> deletePackage(int id)
	{
		if(packFile.deletePackage(id)!=null)
		{
			setPackages();
			return packs;
		}else return null;
	}
	
	
	@WebMethod
	public String updatePackage(Package Package)
	{
		
		String str="";
		int index=-1;
		Package copyPackage=null;
		User arr=null,dep=null;
		boolean ok1=false,ok2=false;
		
		for(int i=0;i<packs.size();i++)
			if(packs.get(i).getId()==Package.getId())
			{
				
				copyPackage=packs.get(i);
				index=i;
			}
		
		
		if(Package.getReceiver()==null||!copyPackage.getReceiver().equals(Package.getReceiver()))
		{
			
			for(User c:users)	
			{
				if(c.getId()==Package.getReceiverId())
				{
					ok1=true;
					arr=c;
				}
			}
			if(!ok1)
				str+=" Receiver user invalid! "+ Package.getReceiverId()+" ";
		}else ok1=true;
		
		if(Package.getSender()==null||!copyPackage.getSender().equals(Package.getSender()))
		{
			
			for(User c:users)	
			{
				if(c.getId()==Package.getSenderId())
				{
					dep=c;
					ok2=true;
				}
			}
			if(!ok2)
				str+=" Sender user invalid! "+ Package.getSenderId()+" ";
		}else ok2=true;
		
		if(ok1&&ok2)
		{
			packFile.updatePackage(Package);
			packs.set(index,packFile.findPackage(Package.getId()));
			packs.get(index).setReceiver(arr);
			packs.get(index).setSender(dep);
		}
		return str;
		///this.packs = (ArrayList<model.Package>) packFile.findPackages();
	}
	
	@WebMethod
	public String insertPackage(Package Package)
	{
		String str="";
		int ok=0;
		User arr=null,dep=null;
		for(User c:users)	
		{
			if(c.getId()==Package.getReceiverId())
			{
				ok++;
				arr=c;
			}
		}
		if(ok<1)
			str+=" Receiver user invalid! "+ Package.getReceiverId()+" ";
		ok=0;
		for(User c:users)	
		{
			if(c.getId()==Package.getSenderId())
			{
				ok++;
				dep=c;
			}
	
		}
		if(ok<1)
			str+=" Departure user invalid! "+ Package.getSenderId()+" ";
		if(str=="")
		{
			Package=packFile.addPackage(Package);
			if(Package.getId()!=0)
			{
				Package.setSender(dep);
				Package.setReceiver(arr);
				packs.add(Package);
			}
			
		}
		//this.packs = (ArrayList<model.Package>) packFile.findPackages();
		return str;
	}
	
	@WebMethod
	public void populate()
	{
		try {
			packFile.populate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setPackages();
	}
	/*
	public static void main(String[] args) {
		
		PackageService temp=new PackageService();
		//System.out.println(temp.getBooks());
		
		//temp.populate();
		//temp.setPackages();
		//System.out.println(temp.searchPackage("a"));
		try {
			
			System.out.println(temp.getTable(temp.getPackages()));
			Package usr=new Package();
	
			usr.setType("boeing");
			usr.setNumber(46);
			usr.setArrivalUser("Tokyo");
			usr.setDepartureUser("Cluj");
			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
			String dateInString = "21-01-2017 10:20:56";
			Date date = sdf.parse(dateInString);
			usr.setDepartureTime(date);
			usr.setId(10);
			dateInString = "21-01-2017 18:30:36";
			date = sdf.parse(dateInString);
			usr.setArrivalTime(date);
			
			System.out.println(temp.updatePackage(usr));
			//System.out.println(temp.deletePackage("dfsrgblcvb"));
			System.out.println(temp.getTable(temp.getPackages()));
			
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
		
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

*/

}
