package service;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.hibernate.cfg.Configuration;

import entities.User;
import repositories.UserRepository;

@WebService
public class UserService{
	private List<User> users;
	private UserRepository empFile;
	

	public UserService()
	{
		
		this.empFile=new UserRepository(new Configuration().configure().buildSessionFactory());;
		this.users=(ArrayList<User>) this.empFile.findUsers();
	}
	

	@WebMethod
	public List<User> getUsers() {
		return new ArrayList<User>(users);
	}
	@WebMethod
	public List<String> getUsernames()
	{
		List<String> names=new ArrayList<String>();
		for(User u:users)
			names.add(u.getUsername());
		return names;
	}
	@WebMethod
	public List<String> getPasswords()
	{
		List<String> passwords=new ArrayList<String>();
		for(User u:users)
			passwords.add(u.getPassword());
		return passwords;

	}
	@WebMethod
	public String printUsers() {
		return users.toString();
	}
	@WebMethod
	protected void setUsers(List<User> emps) {
		this.users = emps;
	}
	@WebMethod
	protected void setUsers() {
		this.users = (List<User>) empFile.findUsers();
	}
	
	@WebMethod
	public List<User> searchUser(String name)
	{
		List<User> rez=new ArrayList<User>();
		for(User e:users)
			if(e.getName().contains(name))
				rez.add(e);
		
		return rez;
	}
	@WebMethod
	public List<User> searchName(String name)
	{
		List<User> rez=new ArrayList<User>();
		for(User e:users)
			if(e.getName().contains((name)))
				rez.add(e);
		
		return rez;
	}
	@WebMethod
	public List<User> searchPassword(String name)
	{
		ArrayList<User> rez=new ArrayList<User>();
		for(User e:users)
			if(e.getPassword().equals((name)))
				rez.add(e);
		
		return rez;
	}
	
	@WebMethod
	public boolean existsAdmin()
	{
		for(User e:users)
			if(e.getAdmin())
				return true;
		
		return false;
	}
	@WebMethod
	public User getAdmin()
	{
		for(User e:users)
			if(e.getAdmin())
				return e;
		
		return null;
	}

	@WebMethod
	public List<User> deleteUser(int id)
	{
		empFile.deleteUser(id);
		
			this.users = empFile.findUsers();
		return users;
	}
	@WebMethod
	public Set<String> getDeclaredFields()
	{
		Set<String> fields=new HashSet<String>();
		
		Field[] empCols=User.class.getDeclaredFields();
		int nrCols=empCols.length;
		for(int i=0;i<nrCols;i++)
			fields.add(empCols[i].getName().substring(0, 1).toUpperCase()+empCols[i].getName().substring(1));
		
		return fields;
	}

	@WebMethod
	public void updateUser(User User)
	{
		empFile.updateUser(User);
		this.users = empFile.findUsers();
	}
	@WebMethod
	public void insertUser(User User)
	{
		empFile.addUser(User);
		this.users = empFile.findUsers();
	}
	/*@WebMethod
	public void insert(HashMap<String,String> info) throws NumberFormatException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		User temp=new User();
		Method[] empMethods=User.class.getDeclaredMethods();
		for(int n=0;n<empMethods.length;n++)
		{
			if(empMethods[n].getName().equals("setId"))
			{
				
			}else
				if(empMethods[n].getName().equals("setAdmin"))
				{
					empMethods[n].invoke(temp,Boolean.parseBoolean(info.get(empMethods[n].getName().substring(3))));
				}else
			if(empMethods[n].getName().startsWith("set"))
			{
				empMethods[n].invoke(temp,info.get(empMethods[n].getName().substring(3)));
			}
		}
		empFile.addUser(temp);
	}*/
	@WebMethod
	public void populate()
	{
		empFile.populate();
		this.users =  empFile.findUsers();
	}
	/*
	public static void main(String[] args) {
		
		UserService temp=new UserService();
		//System.out.println(temp.getBooks());
		
		//temp.populate();
		//System.out.println(temp.searchUser("a"));
		try {
			//User usr=temp.searchNpc("1234567890");
			//usr.setAdmin(true);
			//temp.updateUser(usr);
			System.out.println(temp.getTable(temp.getUsers()));
			System.out.println(temp.getDeclaredFields());
			//System.out.println(temp.deleteUser("dfsrgblcvb"));
			
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
		
			e.printStackTrace();
		}
		
	}*/
}