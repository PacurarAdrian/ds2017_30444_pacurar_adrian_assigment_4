package entities;

import java.util.Date;

public class Route {
	private int id;
	private String city;
	private Date time;
	private int packageId;
	
	
	public String toString()
	{
		return "Route entry for pakage "+packageId+" has \ncity: "+city+"\ntime: "+time+"\n";
	}

	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public Date getTime() {
		return time;
	}


	public void setTime(Date time) {
		this.time = time;
	}
	public int getPackageId() {
		return packageId;
	}


	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}


	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	

}
