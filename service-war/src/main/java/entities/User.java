/**
 * 
 */
package entities;

/**
 * @author Adi
 *
 */

public class User {

	private String name;
	private String username;
	private String password;
	private boolean admin;	
	private int id;
	
	public User(String name,String username,String password,boolean admin)
	{
		this.name=name;
		
		this.username=username;
		this.password=password;
		this.admin=admin;
	}
	public User()
	{
		
	}
	@Override
	public String toString()
	{
		return id+": Employee is called "+name+", has username "+username+", password "+password;
	}
	
	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}


	public String getUsername() {
		return username;
	}




	public void setUsername(String username) {
		this.username = username;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public boolean getAdmin() {
		return admin;
	}




	public void setAdmin(boolean admin) {
		this.admin = admin;
	}




	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}

	

	

}
