package entities;


public class Package {
	private int id;
	private String name;
	private User sender;
	private User receiver;
	private int senderId;
	private int receiverId;
	private String Description;
	private String destinationCity;
	private String senderCity;
	private boolean tracking;
	
	public Package()
	{
		this("",0,0,"","",false);
	}
	public Package(String name,int senderId,int receiverId,String destinationCity,String senderCity,boolean tracking)
	{
		
		this.name=name;
		this.senderId=senderId;
		this.receiverId=receiverId;
		this.destinationCity=destinationCity;
		this.senderCity=senderCity;
		this.tracking=tracking;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public User getSender() {
		return sender;
	}
	public void setSender(User sender) {
		this.sender = sender;
	}
	public int getReceiverId() {
		return receiverId;
	}
	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}
	public int getSenderId() {
		return senderId;
	}
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	public User getReceiver() {
		return receiver;
	}
	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	public String getSenderCity() {
		return senderCity;
	}
	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}
	public boolean getTracking() {
		return tracking;
	}
	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	
	
	
	
	
}
