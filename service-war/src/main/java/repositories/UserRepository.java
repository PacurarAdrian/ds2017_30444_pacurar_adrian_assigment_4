/**
 * 
 */
package repositories;

import java.util.List;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import entities.User;


/**
 * @author Adi
 *
 */
public class UserRepository {
	
//		private static final Log LOGGER = LogFactory.getLog(UserDAO.class);

		private SessionFactory factory;

		public UserRepository(SessionFactory factory) {
			this.factory = factory;
		}

		public User addUser(User User) {
			int UserId = -1;
			Session session = factory.openSession();
			Transaction tx = null;
			try {
				tx = session.beginTransaction();
				UserId = (Integer) session.save(User);
				User.setId(UserId);
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
//				LOGGER.error("", e);
			} finally {
				session.close();
			}
			return User;
		}

		@SuppressWarnings("unchecked")
		public List<User> findUsers() {
			Session session = factory.openSession();
			Transaction tx = null;
			List<User> Users = null;
			try {
				tx = session.beginTransaction();
				Users = session.createQuery("FROM User").list();
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
///				LOGGER.error("", e);
			} finally {
				session.close();
			}
			return Users;
		}

		@SuppressWarnings("unchecked")
		public User findUser(int id) {
			Session session = factory.openSession();
			Transaction tx = null;
			List<User> Users = null;
			try {
				tx = session.beginTransaction();
				Query query = session.createQuery("FROM User WHERE id = :id");
				query.setParameter("id", id);
				Users = query.list();
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
//				LOGGER.error("", e);
			} finally {
				session.close();
			}
			return Users != null && !Users.isEmpty() ? Users.get(0) : null;
		}
		@SuppressWarnings("unchecked")
		public User deleteUser(int id) {
			Session session = factory.openSession();
			Transaction tx = null;
			int result=0;
			List<User> Users = null;
			try {
				tx = session.beginTransaction();
				Query query = session.createQuery("FROM User WHERE id = :id");
				query.setParameter("id", id);
				Users = query.list();
				query = session.createQuery("delete User WHERE id = :id");
				query.setParameter("id", id);
				result = query.executeUpdate();
				if (result > 0) {
				    System.out.println("Expensive products was removed");
				}
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
	//			LOGGER.error("", e);
			} finally {
				session.close();
			}
			return Users != null && !Users.isEmpty() ? Users.get(0) : null;
		}
		@SuppressWarnings("unchecked")
		public User deleteUser(String npc) {
			Session session = factory.openSession();
			Transaction tx = null;
			int result=0;
			List<User> Users = null;
			try {
				tx = session.beginTransaction();
				Query query = session.createQuery("FROM User WHERE PNC = :pnc");
				query.setParameter("pnc", npc);
				Users = query.list();
				query = session.createQuery("delete User WHERE PNC = :pnc");
				query.setParameter("pnc", npc);
				result = query.executeUpdate();
				if (result > 0) {
				    System.out.println("User "+Users.get(0)+" was removed");
				}
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
//				LOGGER.error("", e);
			} finally {
				session.close();
			}
			return Users != null && !Users.isEmpty() ? Users.get(0) : null;
		}
	public void updateUser(User user) {
	
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(user);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
//			LOGGER.error("", e);
		} finally {
			session.close();
		}
		
		
	}
	public void populate() {
			
			User emp = new User();
			emp.setName("Pankaj");
			emp.setUsername("CEO");
			emp.setPassword("parola");
			emp.setAdmin(false);
			
			this.addUser(emp);
			emp=new User();
			emp.setName("Dumitru Ion");
			
			emp.setPassword("pass");
			//emp.setSalary("3253");
			emp.setUsername("dumi");
			emp.setAdmin(false);
			//emp.setHireDate("1993-01-23");
			addUser(emp);
			emp=new User();
			//emp.setHireDate("2003-01-02");
			emp.setName("Kona Diana");
			
			emp.setPassword("passparola");
			//emp.setSalary("5472");
			emp.setUsername("didiana");
			emp.setAdmin(false);
			addUser(emp);
			emp=new User();
			emp.setName("Stefanescu Delavrancea");
			
			emp.setPassword("deundemam");
			//emp.setSalary("2653");
			emp.setUsername("std_def");
			emp.setAdmin(false);
			//emp.setHireDate("2013-01-12");
			addUser(emp);
			
		}

		
		public static void main(String[] args) {
			UserRepository studentDao = new UserRepository(new Configuration().configure().buildSessionFactory());
			/*User emp = new User();
			emp.setName("Pankaj");
			emp.setUsername("CEO");
			emp.setPassword("parola");
			emp.setAdmin(false);
			emp.setNpc("1234567890");
			
			studentDao.addUser(emp);*/
			System.out.println(studentDao.findUser(0));
		}

	
}
