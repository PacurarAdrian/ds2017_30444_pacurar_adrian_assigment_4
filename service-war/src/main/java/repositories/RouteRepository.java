package repositories;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import entities.Route;

public class RouteRepository {
	//private static final Log LOGGER = LogFactory.getLog(RouteDAO.class);

	private SessionFactory factory;

	public RouteRepository(SessionFactory factory) {
		this.factory = factory;
	}

	public Route addRoute(Route Route) {
		int RouteId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			RouteId = (Integer) session.save(Route);
			Route.setId(RouteId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			//LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Route;
	}

	@SuppressWarnings("unchecked")
	public List<Route> findRoutes() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Route> Routes = null;
		try {
			tx = session.beginTransaction();
			Routes = session.createQuery("FROM Route").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		//	LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Routes;
	}

	@SuppressWarnings("unchecked")
	public Route findRoute(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Route> Routes = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Route WHERE id = :id");
			query.setParameter("id", id);
			Routes = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
//			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Routes != null && !Routes.isEmpty() ? Routes.get(0) : null;
	}
	@SuppressWarnings("unchecked")
	public Route deleteRouteId(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		int result=0;
		List<Route> Routes = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Route WHERE id = :id");
			query.setParameter("id", id);
			Routes = query.list();
			query = session.createQuery("delete Route WHERE id = :id");
			query.setParameter("id", id);
			result = query.executeUpdate();
			if (result > 0) {
			    System.out.println("Expensive products was removed");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
//			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Routes != null && !Routes.isEmpty() ? Routes.get(0) : null;
	}
	@SuppressWarnings("unchecked")
	public List<Route> deleteRoutePackage(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		int result=0;
		List<Route> Routes = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Route WHERE PackageId = :id");
			query.setParameter("id", id);
			Routes = query.list();
			query = session.createQuery("delete Route WHERE PackageId = :id");
			query.setParameter("id", id);
			result = query.executeUpdate();
			if (result > 0) {
			    System.out.println("Expensive products was removed");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
//			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Routes != null && !Routes.isEmpty() ? Routes : null;
	}
	@SuppressWarnings("unchecked")
	public List<Route> deleteRoute(String name) {
		Session session = factory.openSession();
		Transaction tx = null;
		int result=0;
		List<Route> Routes = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Route WHERE City = :name");
			query.setParameter("name", name);
			Routes = query.list();
			query = session.createQuery("delete Route WHERE City = :name");
			query.setParameter("pnc", name);
			result = query.executeUpdate();
			if (result > 0) {
			    System.out.println("Route "+Routes.get(0)+" was removed");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
//			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Routes != null && !Routes.isEmpty() ? Routes : null;
	}
public void updateRoute(Route route) {

	Session session = factory.openSession();
	Transaction tx = null;
	try {
		tx = session.beginTransaction();
		session.saveOrUpdate(route);
		tx.commit();
	} catch (HibernateException e) {
		if (tx != null) {
			tx.rollback();
		}
//		LOGGER.error("", e);
	} finally {
		session.close();
	}
	
	
}
public void populate() throws ParseException {
		
		Route route = new Route();
		route.setCity("Cluj");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String dateInString = "21-01-2017 10:20:56";
		Date date = sdf.parse(dateInString);
		route.setTime(date);
		route.setPackageId(1);
		this.addRoute(route);
		
		route=new Route();
		route.setCity("London");
		dateInString = "15-01-2017 10:20:56";
		date = sdf.parse(dateInString);
		route.setTime(date);
		route.setPackageId(2);
		addRoute(route);
		
		route=new Route();
		route.setCity("Tokyo");
		dateInString = "01-01-2017 12:20:56";
		date = sdf.parse(dateInString);
		route.setTime(date);
		route.setPackageId(3);
		addRoute(route);
		
		
		route=new Route();
		route.setCity("Moscow");
		dateInString = "01-01-2017 12:20:56";
		date = sdf.parse(dateInString);
		route.setTime(date);
		route.setPackageId(3);
		addRoute(route);
		
	}

	
	public static void main(String[] args) {
		RouteRepository routeDao = new RouteRepository(new Configuration().configure().buildSessionFactory());
				
		try {
			routeDao.populate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(route.getId());
	}



}
