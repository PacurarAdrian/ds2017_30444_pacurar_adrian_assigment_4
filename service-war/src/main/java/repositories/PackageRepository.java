package repositories;

import java.text.ParseException;
import java.util.List;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import entities.Package;

public class PackageRepository {

//	private static final Log LOGGER = LogFactory.getLog(PackageDAO.class);

	private SessionFactory factory;

	public PackageRepository(SessionFactory factory) {
		this.factory = factory;
	}

	public Package addPackage(Package Package) {
		int PackageId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			PackageId = (Integer) session.save(Package);
			Package.setId(PackageId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
	//		LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Package;
	}

	@SuppressWarnings("unchecked")
	public List<Package> findPackages() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Package> Packages = null;
		try {
			tx = session.beginTransaction();
			Packages = session.createQuery("FROM Package").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
//			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Packages;
	}

	@SuppressWarnings("unchecked")
	public Package findPackage(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Package> Packages = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Package WHERE id = :id");
			query.setParameter("id", id);
			Packages = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
//			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Packages != null && !Packages.isEmpty() ? Packages.get(0) : null;
	}
	@SuppressWarnings("unchecked")
	public Package deletePackage(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		int result=0;
		List<Package> Packages = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Package WHERE id = :id");
			query.setParameter("id", id);
			Packages = query.list();
			query = session.createQuery("delete Package WHERE id = :id");
			query.setParameter("id", id);
			result = query.executeUpdate();
			if (result > 0) {
			    System.out.println("Package was removed");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
//			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Packages != null && !Packages.isEmpty() ? Packages.get(0) : null;
	}
	@SuppressWarnings("unchecked")
	public Package deletePackage(String type) {
		Session session = factory.openSession();
		Transaction tx = null;
		int result=0;
		List<Package> packs = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Package WHERE Airplane type = :type");
			query.setParameter("type", type);
			packs = query.list();
			query = session.createQuery("delete Package WHERE Airplane type = :type");
			query.setParameter("type", type);
			result = query.executeUpdate();
			if (result > 0) {
			    System.out.println("Packages "+packs+" were removed");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
	//		LOGGER.error("", e);
		} finally {
			session.close();
		}
		return packs != null && !packs.isEmpty() ? packs.get(0) : null;
	}
public void updatePackage(Package pack) {

	Session session = factory.openSession();
	Transaction tx = null;
	try {
		tx = session.beginTransaction();
		session.saveOrUpdate(pack);
		tx.commit();
	} catch (HibernateException e) {
		if (tx != null) {
			tx.rollback();
		}
		//LOGGER.error("", e);
	} finally {
		session.close();
	}
	
	
}
public void populate() throws ParseException {
		
		Package pack = new Package("pack1",1,2,"Cluj","Tokyo",false);
	
		this.addPackage(pack);
		
		pack=new Package("pack2",3,4,"Moscow","London",false);
		
		addPackage(pack);
		
		pack=new Package("pack3",2,3,"Tokyo","Moscow",false);
		
		addPackage(pack);
		
		pack=new Package("pack4",2,1,"London","Cluj",false);
		
		
		addPackage(pack);
		
	}

	
	public static void main(String[] args) throws ParseException {
		PackageRepository PackageDao = new PackageRepository(new Configuration().configure().buildSessionFactory());
		
		
		PackageDao.populate();
		
	}



}
