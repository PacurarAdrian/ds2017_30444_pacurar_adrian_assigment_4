package errorHandler;

import java.util.List;


public class EntityValidationException extends RuntimeException {
    List<String> validationErrors;
    private static final long serialVersionUID = 1L;

    public EntityValidationException(String msg, List<String> errors) {
        super(msg);
        this.validationErrors = errors;
    }

    public List<String> getValidationErrors() {
        return validationErrors;
    }
}
