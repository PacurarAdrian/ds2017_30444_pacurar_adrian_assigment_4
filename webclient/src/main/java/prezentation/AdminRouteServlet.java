package prezentation;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceRef;

import errorHandler.EntityValidationException;
import model.endpoint.UserService;
import model.endpoint.PackageService;
import model.endpoint.PackageServiceService;
import model.endpoint.Package;
import model.endpoint.Route;
import model.endpoint.RouteService;
import model.endpoint.RouteServiceService;
import model.endpoint.UserServiceService;
/**
 * Servlet implementation class AdminServlet
 */
@WebServlet(
        description = "Admin User Servlet", 
       urlPatterns = {"/AdminRouteServlet"})
public class AdminRouteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/PackageServiceService?wsdl")
	private PackageServiceService packages; 
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/RouteServiceService?wsdl")
	private RouteServiceService routeModule; 
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/UserServiceService?wsdl")
	private UserServiceService users;
	//private ReportTableModule report;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminRouteServlet() {
        super();
        System.out.println("constructor---->in");
        
        System.out.println("constructor---->out");
        
    }
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession(true);
		 UserService users=this.users.getUserServicePort();
		 RouteService routes=this.routeModule.getRouteServicePort();
    	if(session.getAttribute("id")==null||Integer.valueOf(users.getAdmin().getId())!=session.getAttribute("id"))
		{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
	    	dispatcher.forward(request,response);
		}else
		{	
			try {
				session.setAttribute("packageRouteId", null);
				request.setAttribute("routeData", routes.getRoutes());
				
				
			} catch (IllegalArgumentException e) {
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
			
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditRoute.jsp");
        	dispatcher.forward(request,response);
			
		}   	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */ 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		 UserService users=this.users.getUserServicePort();
		 RouteService routes=this.routeModule.getRouteServicePort();
		 PackageService packs=this.packages.getPackageServicePort();
    	if(session.getAttribute("id")==null||Integer.valueOf(users.getAdmin().getId())!=session.getAttribute("id"))
    	{

    		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
        	dispatcher.forward(request,response);
    	
    	}else
		if(request.getParameter("oSearch")!=null)
		{
			 
			String temp = request.getParameter("opValue");
			session.setAttribute("packageRouteId", null);
			 
			 System.out.println("temp= "+temp);
			 try{
				 if(temp==null||temp.equals(""))
			     {
					 //request.setAttribute("table", Service.getTable(users.getUsers()));
					
					request.setAttribute("routeData",routes.getRoutes());
			     }
			   	
			     else {
			    	 //request.setAttribute("table",Service.getTable(users.searchName(temp)));
			    	 
			    	 	request.setAttribute("prevSearch",temp);
						List<Package> packRez=packs.searchPackageName(temp);
						List<Route> rez=new ArrayList<Route>();
						for(Package r:packRez)
						{
							int id=r.getId();
							rez.addAll(routes.searchRoutes(id));
						}
						request.setAttribute("routeData",rez);
			     }
						    
			 }
			 catch(Exception e){
				
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
				
			 
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditRoute.jsp");
		    dispatcher.include(request,response);
			
		}else 

		if(request.getParameter("oEdit")!=null)
		{
			
			try {
				
				int id=Integer.parseInt(request.getParameter("elemId"));
				System.out.println("id de update:"+id);
				Route f=routes.getRoute(id);
			
				//f.setId(id);
				f.setCity(request.getParameter("city"));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh:mm");
				String dateInString = request.getParameter("time");
				dateInString=dateInString.replace('T',' ');
				Date date = sdf.parse(dateInString);
				
				
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(date);
				XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
//				date2.toGregorianCalendar().getTime()
				f.setTime(date2);
				routes.updateRoute(f);
				
				
				 if(session.getAttribute("packageRouteId")!=null)
				{
					int idroute=(Integer)session.getAttribute("packageRouteId");
					request.setAttribute("routeData", routes.searchRoutes(idroute));
				}else request.setAttribute("routeData",routes.getRoutes());
				
				 
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("message", "Problem occured while trying to edit data!");
				
			}
			
		    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditRoute.jsp");
		    dispatcher.include(request,response);
			
		}
    	if(request.getParameter("oInsert")!=null)
		{
			
			try {
				
				Route f=new Route();
				if(session.getAttribute("packageRouteId")==null)
					throw new EntityValidationException("Cannot insert without package information",
							Arrays.asList("packageId is null"));
				int idroute=(Integer)session.getAttribute("packageRouteId");
				f.setPackageId(idroute);
				Package p=packs.searchPackageId(idroute).get(0);
				if(!p.isTracking())
					throw new EntityValidationException("Cannot add route to nontracked package",
							Arrays.asList("invalid operation"));
				f.setCity(request.getParameter("city"));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh:mm");
				String dateInString = request.getParameter("time");
				dateInString=dateInString.replace('T',' ');
				Date date = sdf.parse(dateInString);
				
				
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(date);
				XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
				
				f.setTime(date2);
				routes.insertRoute(f);
				
				
				 request.setAttribute("routeData", routes.searchRoutes(idroute));
			}catch(EntityValidationException e)
			{
				 request.setAttribute("routeData", routes.getRoutes());
				 request.setAttribute("message",e.getMessage());
				 
			}catch(ParseException e)
			{
				request.setAttribute("message","Invalid date inserted");
				request.setAttribute("prevCity",request.getParameter("city") );
				int idroute=(Integer)session.getAttribute("packageRouteId");
				request.setAttribute("routeData", routes.searchRoutes(idroute));
				
			} catch (Exception e) {
				e.printStackTrace();
				
				request.setAttribute("message", "Problem occured while trying to edit data!\n"+e.getMessage());
				
			}
			
		    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditRoute.jsp");
		    dispatcher.include(request,response);
			
		}else
    	if(request.getParameter("oDelete")!=null)
		{
			try{
				int id=Integer.parseInt(request.getParameter("elemId"));
				System.out.println("id de sters:"+id);
				
				if(routes.deleteRoute(id)==null)
					request.setAttribute("message","Could not delete route!");
				
				if(session.getAttribute("packageRouteId")!=null)
				{
					int idroute=(Integer)session.getAttribute("packageRouteId");
					request.setAttribute("routeData", routes.searchRoutes(idroute));
				}else request.setAttribute("routeData",routes.getRoutes());
				
			}catch(Exception e)
			{
				e.printStackTrace();
				request.setAttribute("message", "Problem occured while trying to edit data!");
			}
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditRoute.jsp");
		    dispatcher.include(request,response);
			
			
		}else
		if(request.getParameter("oEditRoute")!=null)//display route table button pressed
		{
			
			try {
				
				
				
				int id=Integer.parseInt(request.getParameter("elemId"));
				request.setAttribute("routeData",routes.searchRoutes(id));
				session.setAttribute("packageRouteId", id);
				
				
			}catch (Exception e) {
				 e.printStackTrace();
				 request.setAttribute("message", "Problem occured while trying to fetch data!");
				
			}
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditRoute.jsp");
        	dispatcher.forward(request,response);
			
		}
		
    	
		
    	
		
	}

}
