package prezentation;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;


import model.endpoint.User;

import model.endpoint.UserService;
import model.endpoint.UserServiceService;
import model.endpoint.PackageService;
import model.endpoint.PackageServiceService;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(
        description = "Login Servlet", 
       urlPatterns = { "/" }
     
//       , initParams = { 
//                @WebInitParam(name = "user", value = "Pankaj"), 
//                @WebInitParam(name = "password", value = "journaldev")
//        }
       )
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/PackageServiceService?wsdl")
	private PackageServiceService packages; 
	@WebServiceRef(wsdlLocation =   "http://localhost:8080/service-war/UserServiceService?wsdl")
	private UserServiceService module;
    /**
     * @see HttpServlet#HttpServlet()
     */ 
    public LoginServlet() {
        super();
		
    }
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		if(session.getAttribute("name")!=null)
    	{
			session.setAttribute("name", null);
			session.setAttribute("id", null);
			request.setAttribute("message", "You have been logged out successfully");
			
    	}
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
        rd.include(request, response);
       
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
        PackageService packages=this.packages.getPackageServicePort(); 
        UserService users=this.module.getUserServicePort();
        if(request.getParameter("oRegister")!=null)
		{
        	//request.setAttribute("keys",users.getDeclaredFields());
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/register.jsp");
        	dispatcher.include(request,response);
		}else 
		if(request.getParameter("oApply")!=null)
		{
			
			try {
				
				
				User f=new User();
				
				f.setAdmin(Boolean.parseBoolean(request.getParameter("admin")));
				f.setName(request.getParameter("name"));
				
				f.setPassword(request.getParameter("password"));
				f.setUsername(request.getParameter("username"));
				
				users.insertUser(f);
				
				
				request.setAttribute("message", "Register succesfull!");
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("exception","Invalid input detected:"+e.getMessage()+"please reenter data");
				 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
			     dispatcher.forward(request,response);
			}
			
			
			
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
            rd.include(request, response);    
		}
        else{
			User rez = null;
			try {
				rez = users.getAdmin();
				System.out.println(rez);
				 //get request parameters for userID and password
				String user = request.getParameter("user");
		        String pwd = request.getParameter("pwd");
		    	String userID = rez.getUsername();
		        String password = rez.getPassword();
		        List<String> names = users.getUsernames();
		        List<String> passwords = users.getPasswords();
			    
			    //System.out.println(users.getDeclaredFields());
			 
		        //logging example
		        log("User="+user+"::password="+pwd);
		         
		        if(userID.equalsIgnoreCase(user) && password.equals(pwd)){
		        	//request.setAttribute("user", user);
		        	HttpSession session = request.getSession(true);
		        	session.setAttribute("name", rez.getName());
		        	session.setAttribute("id", rez.getId());
		        	request.setAttribute("userData", users.getUsers());
		        	request.setAttribute("packData",packages.getPackages());
		        	/*List<User> li=users.getUsers();
		        	//System.out.println(li);
		        	HashMap<String,ArrayList<String>> tab=Service.getTable(li);
					request.setAttribute("table",tab);
					System.out.println(tab);*/
					
					//System.out.println(flights.searchPackageName("1").get(0).getReceiver().toString());
		        	//List<model.endpoint.Package> li=packages.getPackages();
		        	
	            	//request.setAttribute("packages",Service.getTable(li));
	            	
		        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminHome.jsp");
		        	dispatcher.include(request,response);
		            //response.sendRedirect("LoginSuccess.jsp");
		        }else{
		        	if(names.contains(user) && passwords.contains(pwd))
		        	{
		        		HttpSession session = request.getSession(true);
		        		User u=users.searchPassword(pwd).get(0);
		            	session.setAttribute("name", u.getName());
		            	session.setAttribute("id",u.getId());
		            	request.setAttribute("packData",packages.searchPackageSnd(u.getId()));
		            	//request.setAttribute("table",Service.getTable(packages.searchPackageSnd(u.getId())));
						//System.out.println(module.getTable(module.getUsers()));
		        		//request.setAttribute("client", user);
		            	//request.setAttribute("tableName","Packages");
		            	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userHome.jsp");
		            	dispatcher.forward(request,response);
		        	}else
		        	{
			            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
			            request.setAttribute("message", "Either username or password is wrong!");
			            rd.include(request, response);
			        }
		             
		        }
			} catch (Exception e) {
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
	            request.setAttribute("message", "Exception occured while trying to connect to database!");
	            rd.include(request, response);
				e.printStackTrace();
			}
        }
	}

}
