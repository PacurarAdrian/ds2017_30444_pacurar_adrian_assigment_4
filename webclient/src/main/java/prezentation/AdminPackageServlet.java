package prezentation;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;

import model.endpoint.UserService;
import model.endpoint.PackageService;
import model.endpoint.PackageServiceService;
import model.endpoint.RouteService;
import model.endpoint.RouteServiceService;
import model.endpoint.Package;
import model.endpoint.UserServiceService;
/**
 * Servlet implementation class AdminServlet
 */
@WebServlet(
        description = "Admin User Servlet", 
       urlPatterns = {"/AdminPackageServlet"})
public class AdminPackageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/PackageServiceService?wsdl")
	private PackageServiceService packages; 
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/RouteServiceService?wsdl")
	private RouteServiceService routeModule; 
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/UserServiceService?wsdl")
	private UserServiceService users;
	//private ReportTableModule report;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminPackageServlet() {
        super();
        System.out.println("constructor---->in");
        
        System.out.println("constructor---->out");
        
    }
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession(true);
		 UserService users=this.users.getUserServicePort();
		 PackageService packages=this.packages.getPackageServicePort();
		
    	if(session.getAttribute("id")==null||Integer.valueOf(users.getAdmin().getId())!=session.getAttribute("id"))
		{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
	    	dispatcher.forward(request,response);
		}else
		if(request.getParameter("edit")!=null)
		{
			try {
				
				request.setAttribute("packageData", packages.getPackages());
				request.setAttribute("userData", users.getUsers());
				List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
				request.setAttribute("trackingData",list);
				
			} catch (IllegalArgumentException e) {
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
			
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditPackage.jsp");
        	dispatcher.forward(request,response);
		}
		else//on other get request
		{
			try {
				request.setAttribute("message", "Welcome to OTS "+session.getAttribute("name")+"!");
				//int id=Integer.parseInt(String.valueOf( session.getAttribute("id")));
				//request.setAttribute("packData",module.searchPackageSnd(id));
				
			} catch (Exception e) {
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
			
				       
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditPackage.jsp");
        	dispatcher.forward(request,response);
		}
    	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */ 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		 UserService users=this.users.getUserServicePort();
		 PackageService packages=this.packages.getPackageServicePort();
    	if(session.getAttribute("id")==null||Integer.valueOf(users.getAdmin().getId())!=session.getAttribute("id"))
    	{

    		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
        	dispatcher.forward(request,response);
    	
    	}else
		if(request.getParameter("oSearch")!=null)
		{
			 String temp = request.getParameter("opValue");
			
			 
			 
			 try{
				 if(temp==null||temp.equals(""))
			     {
					 request.setAttribute("packageData", packages.getPackages());
					request.setAttribute("userData", users.getUsers());
					List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
					request.setAttribute("trackingData",list);
			     }
			     else {
			    	 
			    	 
			    	  String regex=temp;
			    	  Pattern p = Pattern.compile("[0-9]*");
			  	      Matcher m = p.matcher(regex);
			  	      if (!m.matches())
			  	      {
			  	    		//request.setAttribute("table",Service.getTable(packages.searchPackageName(temp)));
			  	    		request.setAttribute("packageData", packages.searchPackageName(temp));
			  	      }else{
							//request.setAttribute("table", Service.getTable(packages.searchPackageSnd(Integer.parseInt(temp))));
							request.setAttribute("packageData", packages.searchPackageSnd(Integer.parseInt(temp)));
			  	      }
			  	    request.setAttribute("prevSearch",temp);
			  	    request.setAttribute("userData", users.getUsers());
					 List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
					 request.setAttribute("trackingData",list);
			     
			  	    	 
			     }
						    
			 }
			 catch(Exception e){
				//throw new StoreException("User search could not be displayed");
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
				
			 
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditPackage.jsp");
		    dispatcher.include(request,response);
			
		}else 

		if(request.getParameter("oEdit")!=null)
		{
			
			try {
				//deleteUser(users,request);
				int id=Integer.parseInt(request.getParameter("elemId"));
				System.out.println("id de update:"+id);
				Package f=new Package();
				f.setId(id);
				f.setTracking(Boolean.parseBoolean(request.getParameter("tracking")));
				if(!f.isTracking())
				{
					RouteService rs=routeModule.getRouteServicePort();
					rs.deletePackageRoutes(id);
				}
				f.setName(request.getParameter("name"));
				f.setDescription(request.getParameter("description"));
				f.setDestinationCity(request.getParameter("destinationCity"));
				f.setSenderCity(request.getParameter("senderCity"));
				f.setReceiverId(Integer.parseInt(request.getParameter("receiver")));
				f.setSenderId(Integer.parseInt(request.getParameter("sender")));
				
				request.setAttribute("message", packages.updatePackage(f));
				
				List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
				request.setAttribute("trackingData",list);
				request.setAttribute("userData",users.getUsers());
				 request.setAttribute("packageData", packages.getPackages());
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("message", "Problem occured while trying to edit data!");
				
			}
			
		    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditPackage.jsp");
		    dispatcher.forward(request,response);
			
		}
    	if(request.getParameter("oInsert")!=null)
		{
			
			try {
				
				Package f=new Package();
				
				f.setTracking(Boolean.parseBoolean(request.getParameter("tracking")));
				f.setName(request.getParameter("name"));
				f.setDescription(request.getParameter("description"));
				f.setDestinationCity(request.getParameter("destinationCity"));
				f.setSenderCity(request.getParameter("senderCity"));
				f.setReceiverId(Integer.parseInt(request.getParameter("receiver")));
				f.setSenderId(Integer.parseInt(request.getParameter("sender")));
				
				request.setAttribute("message",packages.insertPackage(f));
				
				List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
				request.setAttribute("trackingData",list);
				request.setAttribute("userData",users.getUsers());
				request.setAttribute("packageData", packages.getPackages());
				
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				request.setAttribute("message", "Problem occured while trying to edit data!");
				
			}
			
		    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditPackage.jsp");
		    dispatcher.forward(request,response);
			
		}else
    	if(request.getParameter("oDelete")!=null)
		{
			try{
				int id=Integer.parseInt(request.getParameter("elemId"));
				System.out.println("id de sters:"+id);
				List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
				request.setAttribute("trackingData",list);
				request.setAttribute("packageData",packages.deletePackage(id));
				request.setAttribute("userData",users.getUsers());
				
			}catch(Exception e)
			{
				e.printStackTrace();
				request.setAttribute("message", "Problem occured while trying to edit data!");
			}
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditPackage.jsp");
		    dispatcher.forward(request,response);
			
			
		}
    	
		
	}

}
