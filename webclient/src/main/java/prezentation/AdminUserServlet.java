package prezentation;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;

import model.endpoint.UserService;
import model.endpoint.User;
import model.endpoint.UserServiceService;
/**
 * Servlet implementation class AdminServlet
 */
@WebServlet(
        description = "Admin User Servlet", 
       urlPatterns = {"/AdminUserServlet"})
public class AdminUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/UserServiceService?wsdl")
	private UserServiceService users;
	//private ReportTableModule report;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminUserServlet() {
        super();
        System.out.println("constructor---->in");
        
        System.out.println("constructor---->out");
        
    }
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession(true);
		 UserService users=this.users.getUserServicePort();
		 
		
    	if(session.getAttribute("id")==null||Integer.valueOf(users.getAdmin().getId())!=session.getAttribute("id"))
		{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
	    	dispatcher.forward(request,response);
		}else
		if(request.getParameter("edit")!=null)
		{
			try {
				//request.setAttribute("table",Service.getTable(users.getUsers()));
				//request.setAttribute("tableName",USER);
				List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
				request.setAttribute("adminData",list);
				request.setAttribute("userData", users.getUsers());
				//System.out.println(Service.getTable(users.getUsers()));
			} catch (IllegalArgumentException e) {
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
			
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
        	dispatcher.forward(request,response);
		}
		else//on other get request
		{
			try {
				request.setAttribute("message", "Welcome to OTS "+session.getAttribute("name")+"!");
				//int id=Integer.parseInt(String.valueOf( session.getAttribute("id")));
				//request.setAttribute("packData",module.searchPackageSnd(id));
				
			} catch (Exception e) {
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
			
				       
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
        	dispatcher.forward(request,response);
		}
    	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */ 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		 UserService users=this.users.getUserServicePort();
		
    	if(session.getAttribute("id")==null||Integer.valueOf(users.getAdmin().getId())!=session.getAttribute("id"))
    	{

    		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
        	dispatcher.forward(request,response);
    	
    	}else
		if(request.getParameter("oSearch")!=null)
		{
			 String temp = request.getParameter("opValue");
			
			 
			 System.out.println("temp= "+request.getParameter("tableName"));
			 try{
				 if(temp==null||temp.equals(""))
			     {
					 //request.setAttribute("table", Service.getTable(users.getUsers()));
					List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
					request.setAttribute("adminData",list);
					request.setAttribute("userData",users.getUsers());
			     }
			   	
			     else {
			    	 //request.setAttribute("table",Service.getTable(users.searchName(temp)));
			    	 List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
						request.setAttribute("adminData",list);
						request.setAttribute("userData",users.searchName(temp));
						request.setAttribute("prevSearch",temp);
			     }
						    
			 }
			 catch(Exception e){
				//throw new StoreException("User search could not be displayed");
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
				
			 
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
		    dispatcher.include(request,response);
			
		}else 

		if(request.getParameter("oEditUser")!=null)
		{
			
			try {
				//deleteUser(users,request);
				int id=Integer.parseInt(request.getParameter("elemId"));
				System.out.println("id de update:"+id);
				User f=new User();
				f.setId(id);
				f.setAdmin(Boolean.parseBoolean(request.getParameter("admin")));
				f.setName(request.getParameter("name"));
			
				f.setPassword(request.getParameter("password"));
				f.setUsername(request.getParameter("username"));
				
				users.updateUser(f);
				
				List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
				request.setAttribute("adminData",list);
				request.setAttribute("userData",users.getUsers());
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("message", "Problem occured while trying to edit data!");
				
			}
			
		    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
		    dispatcher.forward(request,response);
			
		}
    	if(request.getParameter("oInsertUser")!=null)
		{
			
			try {
				User f=new User();
				
				f.setAdmin(Boolean.parseBoolean(request.getParameter("admin")));
				f.setName(request.getParameter("name"));
				
				f.setPassword(request.getParameter("password"));
				f.setUsername(request.getParameter("username"));
				
				users.insertUser(f);
				
				List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
				request.setAttribute("adminData",list);
				request.setAttribute("userData",users.getUsers());
				
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				request.setAttribute("message", "Problem occured while trying to edit data!");
				
			}
			
		    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
		    dispatcher.forward(request,response);
			
		}else
    	if(request.getParameter("oDeleteUser")!=null)
		{
			try{
				int id=Integer.parseInt(request.getParameter("elemId"));
				System.out.println("id de sters:"+id);
				List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
				request.setAttribute("adminData",list);
				request.setAttribute("userData",users.deleteUser(id));
				
				
			}catch(Exception e)
			{
				e.printStackTrace();
				request.setAttribute("message", "Problem occured while trying to edit data!");
			}
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
		    dispatcher.forward(request,response);
			
			
		}
		
    	
		//doGet(request,response);
    	
		
	}

}
