package prezentation;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;

import model.endpoint.UserService;
import model.endpoint.PackageService;
import model.endpoint.PackageServiceService;
import model.endpoint.RouteServiceService;
import model.endpoint.UserServiceService;
/**
 * Servlet implementation class AdminServlet
 */
@WebServlet(
        description = "Admin Servlet", 
       urlPatterns = {"/AdminServlet"})
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/PackageServiceService?wsdl")
	private PackageServiceService packageService; 
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/RouteServiceService?wsdl")
	private RouteServiceService routeService; 
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/UserServiceService?wsdl")
	private UserServiceService userService;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        System.out.println("constructor---->in");
        
        System.out.println("constructor---->out");
        
    }
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession(true);
		 UserService users=this.userService.getUserServicePort();
		 PackageService packages=this.packageService.getPackageServicePort();
		
    	if(session.getAttribute("id")==null||Integer.valueOf(users.getAdmin().getId())!=session.getAttribute("id"))
		{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
	    	dispatcher.forward(request,response);
		}else
			if(request.getParameter("home")!=null)
			{
				try {
					
					session.setAttribute("packageRouteId", null);
					request.setAttribute("userData", users.getUsers());
		        	request.setAttribute("packData",packages.getPackages());
					
				} catch (Exception e) {
					request.setAttribute("message", "Problem occured while trying to fetch data!");
					e.printStackTrace();
				}
				
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminHome.jsp");
	        	dispatcher.forward(request,response);
				
			}else
		
			{
				try {
					
					request.setAttribute("userData", users.getUsers());
		        	request.setAttribute("packData",packages.getPackages());
				} catch (Exception e) {
					request.setAttribute("message", "Problem occured while trying to fetch data!");
					e.printStackTrace();
				}
				
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminHome.jsp");
	        	dispatcher.forward(request,response);
			}
    	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */ 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		 UserService users=this.userService.getUserServicePort();
		 PackageService packages=this.packageService.getPackageServicePort();
		 if(session.getAttribute("id")==null||Integer.valueOf(users.getAdmin().getId())!=session.getAttribute("id"))
    	{

    		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
        	dispatcher.forward(request,response);
    	
    	}else
    	{
    		try {
				
				request.setAttribute("userData", users.getUsers());
	        	request.setAttribute("packData",packages.getPackages());
			} catch (Exception e) {
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
			
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminHome.jsp");
        	dispatcher.forward(request,response);
    	}
		
	}

}
