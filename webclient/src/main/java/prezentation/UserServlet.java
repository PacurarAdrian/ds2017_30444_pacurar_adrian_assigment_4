package prezentation;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;


import model.endpoint.PackageService;
import model.endpoint.PackageServiceService;
import model.endpoint.RouteService;
import model.endpoint.RouteServiceService;
/**
 * Servlet implementation class UserServlet
 */
@WebServlet(
		        description = "User Servlet", 
		       urlPatterns = {"/UserServlet"})

public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/PackageServiceService?wsdl")
	private PackageServiceService serviceModule; 
	@WebServiceRef(wsdlLocation =  "http://localhost:8080/service-war/RouteServiceService?wsdl")
	private RouteServiceService routeModule; 
	

	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        System.out.println("constructor---->in");
       // users=new UserService();
        //serviceModule=new PackageServiceService();
        System.out.println("constructor---->out");
        
    }
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PackageService module=this.serviceModule.getPackageServicePort();
		HttpSession session = request.getSession(true);
		if(session.getAttribute("id")==null|| session.getAttribute("name")==null)
		{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
        	dispatcher.forward(request,response);
			
		}else
		if(request.getParameter("home")!=null)
		{
			try {
				int id=Integer.parseInt(String.valueOf(session.getAttribute("id")));
				request.setAttribute("packData",module.searchPackageSnd(id));
				
			} catch (Exception e) {
				
	            request.setAttribute("message", "Problem occured while trying to fetch data!");
	            
				e.printStackTrace();
			}
			
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userHome.jsp");
        	dispatcher.forward(request,response);
			
		}else//on other get request
		{
			try {
				request.setAttribute("message", "Welcome to OTS "+session.getAttribute("name")+"!");
				//int id=Integer.parseInt(String.valueOf( session.getAttribute("id")));
				//request.setAttribute("packData",module.searchPackageSnd(id));
				
			} catch (IllegalArgumentException e) {
				request.setAttribute("message", "Problem occured while trying to fetch data!");
				e.printStackTrace();
			}
			
				       
        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userHome.jsp");
        	dispatcher.forward(request,response);
		}
    	
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */ 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		PackageService module=this.serviceModule.getPackageServicePort();
		if(session.getAttribute("id")==null|| session.getAttribute("name")==null)
		{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
        	dispatcher.forward(request,response);
			
		}else
		if(request.getParameter("oSearch")!=null)//search button pressed
		{
		   String temp = request.getParameter("opValue");
		   if(temp==null||temp.equals(""))// void search value
	       {
	    	
	    	   try{
	    		   int id=Integer.parseInt(String.valueOf(session.getAttribute("id")));
					
					request.setAttribute("packData",module.searchPackageSnd(id));
	    	   }
				catch(Exception e){
					e.printStackTrace();
					 request.setAttribute("message", "Problem occured while trying to fetch data!");
					
					
				}
			    	
	       }else{
	    	   String regex=temp;
	    	   Pattern p = Pattern.compile("[0-9]*");
	  	    	Matcher m = p.matcher(regex);
	  	    	if (!m.matches())//search value is string=> search for package name
	  	    	{
	  	    		try {
	  	    			int id=Integer.parseInt(String.valueOf( session.getAttribute("id")));
						List<model.endpoint.Package> values=module.searchPackageSnd(id);
						if(values!=null)
						{
							for (Iterator<model.endpoint.Package> iterator = values.iterator(); iterator.hasNext();) {
							    
							    if(!iterator.next().getName().toLowerCase().contains(temp.toLowerCase()))
							    {
							        // Remove the current element from the iterator and the list.
							        iterator.remove();
							    }
							}
						}
	  	    			
	  	    			request.setAttribute("packData",values);
					} catch (Exception e) {
						e.printStackTrace();
						 request.setAttribute("message", "Problem occured while trying to fetch data!");
						 
					}
	  	    	}else{//input is integer value => search for receiver id
	    	  
					try {
						 int id=Integer.parseInt(String.valueOf( session.getAttribute("id")));
						List<model.endpoint.Package> values=module.searchPackageSnd(id);
						if(values!=null)
						{
						
							int recvId=Integer.parseInt(temp);
							for (Iterator<model.endpoint.Package> iterator = values.iterator(); iterator.hasNext();) 
							{
							    
							    if(iterator.next().getReceiverId()!=recvId)
							    {
							        // Remove the current element from the iterator and the list.
							        iterator.remove();
							    }
							}
						}
						
						request.setAttribute("packData",values);
					} catch (Exception e) {
						
						 request.setAttribute("message", "Problem occured while trying to fetch data!");
						e.printStackTrace();
						
					}
	  	    	}
	  	    	request.setAttribute("prevSearch",temp); 
	       }
		   
		  
			 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userHome.jsp");
		    	dispatcher.include(request,response);
		}else
		if(request.getParameter("oEditRoute")!=null)//display route table button pressed
		{
			
			try {
				
				
				RouteService routes=this.routeModule.getRouteServicePort();
				int id=Integer.parseInt(request.getParameter("elemId"));
				request.setAttribute("routeData",routes.searchRoutes(id));
				
				
				
			}catch (Exception e) {
				 e.printStackTrace();
				 request.setAttribute("message", "Problem occured while trying to fetch data!");
				
			}
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userRoute.jsp");
        	dispatcher.forward(request,response);
			
		}else{//something else happened
			
			HttpSession session1 = request.getSession(true);
			if(session1.getAttribute("name")!=null)
	    	{
				session1.setAttribute("name", null);
				session1.setAttribute("id", null);
				request.setAttribute("message", "Something went wrong. You have been logged out successfully");
				
	    	}
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
	        rd.include(request, response);
		}
    	
		
		
	}

}
