<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
DIV.table 
{
    display:table;
   border: 1px solid black;
}
FORM.tr, DIV.tr
{
    display:table-row;
}
SPAN.td,form.td
{
    display:table-cell;
    border: 1px solid black;
}
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input[type=case],input[type=number]{
   
 	width: 60px;	
 	
 	 
}
input[type=text]
{
	border-radius: 4px;
}
input[type=submit]{
	width: 60px;
	
}
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}
</style>
<title>OTS Administrator</title>
</head>

<body bgcolor=#D1E0E0>
<table border="0" width=100%>
<tr bgcolor=#D1D0D0><td width=10%></td><td colspan=2><font size=10 face="Viner Hand ITC">Online Tracking System</font></td><td width=10%></td></tr>

<tr>
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55%>
	<fieldset>
		<form method="GET" action="AdminServlet">
			<input type="submit" name="home" value="Home"/>
		</form>
		<form action="AdminPackageServlet" method="POST">
			<input name="opValue" type="text" placeholder="Package name or sender Id"/>
			<input type="submit" name="oSearch" value="Search"/>
			
		 </form>
		 	  
				
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	<form method="get" action="LogInServlet">Logged in: ${name}  <input type="submit" value="Log out"/></form>
		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<details open>
  	<summary><font size=5>Edit Package Table</font></summary>
	<fieldset >
	<legend></legend>
	<div class="table">
	<div class="tr">
		
		<span class="td">Name</span>
		<span class="td">Sender</span>
		<span class="td">Sender City</span>
		<span class="td">Receiver</span>
		<span class="td">Destination City</span>
    	<span class="td">Description</span>
    	<span class="td">Tracking</span>
    	
        <span class="td">Operation</span>
    </div>
    <c:forEach var="element" items="${packageData}">
     <c:set var="id" value="${element.id}"></c:set>
        <form class="tr" action="AdminPackageServlet" method="post">
        	
        	<span class="td"><input type="text" value="${element.name}" name="name"></span>
         
            <span class="td">
            	
            	<select name="sender" >
	           		
	    		<c:forEach var="user" items="${userData}">
	    		<c:choose>
				    <c:when test="${element.sender.id==user.id}">
				        <option value="${user.id}" label="${user.name} id:${user.id}" selected/>
				    </c:when>    
				    <c:otherwise>
				        <option value="${user.id}" label="${user.name} id:${user.id}"/>     
				    </c:otherwise>
				</c:choose>
					
				</c:forEach>
            </select>
            
            </span> 
            <span class="td"><input type="text" value="${element.senderCity}" name="senderCity"></span>
            <span class="td">
			<select name="receiver" >
	           		
	    		<c:forEach var="user" items="${userData}">
	    		<c:choose>
				    <c:when test="${element.receiver.id==user.id}">
				        <option value="${user.id}" label="${user.name}" selected/>
				    </c:when>    
				    <c:otherwise>
				        <option value="${user.id}" label="${user.name}"/>     
				    </c:otherwise>
				</c:choose>
					
				</c:forEach>
            </select>            
            </span>
            <span class="td"><input type="text" value="${element.destinationCity}" name="destinationCity"></span>
            <span class="td"><input type="text" value="${element.description}" name="description"></span>
            <span class="td">
            <select name="tracking" >
	           		
	    		<c:forEach var="admin" items="${trackingData}">
	    		<c:choose>
				    <c:when test="${element.tracking==admin}">
				        <option value="${admin}" label="${admin}" selected/>
				    </c:when>    
				    <c:otherwise>
				        <option value="${admin}" label="${admin}"/>     
				    </c:otherwise>
				</c:choose>
					
				</c:forEach>
            </select>
            
            </span>            
            <span class="td">
            	<input type="hidden" value="${element.id}" name="elemId"> 
                <input type="submit" value="Update" name="oEdit"> 
                <input type="submit" value="Delete" name="oDelete">
            </span> 
        </form>
    </c:forEach>
    
        
    <form  class="tr" action="AdminPackageServlet" method="post">
    
    	<span class="td"><input type="text" name="name" placeholder="name"></span>
    	<span class="td">
    	<select name="sender" >
	    	<c:forEach var="user" items="${userData}">
				<option value="${user.id}" label="${user.name}"/>			
			</c:forEach>
         </select>
    	</span> 
        <span class="td"><input type="text" name="senderCity" placeholder="sender city"></span>
        <span class="td">
        <select name="receiver" >
	    	<c:forEach var="user" items="${userData}">
				<option value="${user.id}" label="${user.name}"/>			
			</c:forEach>
         </select>
        </span>
        <span class="td"><input type="text" name="destinationCity" placeholder="destination city"></span>
        <span class="td"><input type="text" name="description" placeholder="description"></span>
        <span class="td">
            <select name="tracking" >	
				 <option value="false" label="false"/>     
            </select>  
        </span>           
    	<span class="td">
    		<input type="submit" name="oInsert" value="Insert">	
    	</span>
    </form>
	
	</div>
	
	</fieldset>
	</details>
	
	
	</td>
	<td width=10--%> </td>
</tr>
</table>
<script>
	var paramOne ="${message}";
	if(paramOne!=null && paramOne!="" && paramOne.length!=0)
		alert(""+paramOne);
</script>
</body>
</html>