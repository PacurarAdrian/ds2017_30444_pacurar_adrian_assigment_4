<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
body>table, body>table>th,body>table>td {
    border: 0px solid black;
}

body>table{
    width:100%;
}
DIV.table 
{
    display:table;
   border: 1px solid black;
}
FORM.tr, DIV.tr
{
    display:table-row;
}
SPAN.td,form.td
{
    display:table-cell;
    border: 1px solid black;
}
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input[type=case],input[type=number]{
   
 	width: 60px;	
 	
 	 
}
input[type=text]
{
	border-radius: 4px;
}
input[type=submit]{
	width: 60px;
	
}
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}
</style>
<title>OTS Administrator</title>
</head>

<body bgcolor=#D1E0E0>
<table>
<tr bgcolor=#D1D0D0><td width=10%></td><td colspan=2><font size=10 face="Viner Hand ITC">Online Tracking System</font></td><td width=10%></td></tr>

<tr>
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55%>
	<fieldset>
		<form method="GET" action="AdminServlet">
			<input type="submit" name="home" value="Home"/>
		</form>
		<form action="AdminRouteServlet" method="POST">
			<input name="opValue" type="text" value="${prevSearch}" placeholder="Package name"/>
			<input type="submit" name="oSearch" value="Search"/>
			
		 </form>
		 	  
				
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	<form method="get" action="LogInServlet">Logged in: ${name}  <input type="submit" value="Log out"/></form>
		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<details open>
  	<summary><font size=5>Edit Route Table</font></summary>
	<fieldset >
	<legend></legend>
	<div class="table">
	<div class="tr">
		
		<span class="td">Time</span>
    	<span class="td">City</span>
    	
    	
        <span class="td">Operation</span>
    </div>
    <c:forEach var="element" items="${routeData}">
     <c:set var="id" value="${element.id}"></c:set>
        <form class="tr" action="AdminRouteServlet" method="post">
        	

			<fmt:formatDate var="dateObject" value="${element.time.toGregorianCalendar().time}" pattern="yyyy-MM-dd'T'hh:mm" />
        	<span class="td"><input type="datetime-local" value="${dateObject}" name="time"></span>
         
            <span class="td"><input type="text" value="${element.city}" name="city"></span> 
            
            
            
            <span class="td">
            	<input type="hidden" value="${element.id}" name="elemId"> 
                <input type="submit" value="Update" name="oEdit"> 
                <input type="submit" value="Delete" name="oDelete">
            </span> 
        </form>
    </c:forEach>
    
        
    <form  class="tr" action="AdminRouteServlet" method="post">
    
    	<span class="td"><input type="datetime-local" name="time" placeholder="time"></span>
    	<span class="td"><input type="text" name="city" value="${prevCity}" placeholder="city"></span> 
                  
    	<span class="td">
    		<input type="submit" name="oInsert" value="Insert">	
    	</span>
    </form>
	
	</div>
	
	</fieldset>
	</details>
	
	
	</td>
	<td width=10--%> </td>
</tr>
</table>
<script>
	var paramOne ="${message}";
	if(paramOne!=null && paramOne!="" && paramOne.length!=0)
		alert(""+paramOne);
</script>
</body>
</html>