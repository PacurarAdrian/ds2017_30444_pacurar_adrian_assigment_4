<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input[type=text], select{
   
 	
 	border-radius: 4px;
 	 
}
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}
DIV.table 
{
    display:table;
   border: 1px solid black;
}
FORM.tr, DIV.tr
{
    display:table-row;
}
SPAN.td,form.td
{
    display:table-cell;
    border: 1px solid black;
}
</style>
<title>OTS client</title>
</head>

<body bgcolor=#D1E0E0>
<table border="0" width=100%>
<tr bgcolor=#D1D0D0><td width=10%></td><td colspan=2><font size=10 face="Viner Hand ITC">Online Tracking System</font></td><td width=10%></td></tr>

<tr>
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55%>
	<fieldset>
		<form method="GET" action="UserServlet">
			<input type="submit" name="home" value="Home"/>
		</form>
		<form action="UserServlet" method="POST">
			<input name="opValue" type="text" placeholder="Package name or receiver ID"/>
			<input type="submit" name="oSearch" value="Search"/>
			
		 </form>
		 	  
				
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	<form method="get" action="LogInServlet">Logged in: <%=session.getAttribute("name") %>  <input type="submit" value="Log out"/></form>
		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<details open>
  	<summary><font size=5>Packages Table</font></summary>
	<fieldset >
	<legend></legend>
	
	<br>
	
<div class="table">
	<div class="tr">
		<span class="td">Name</span>
		<span class="td">Sender</span>
		<span class="td">Sender City</span>
		<span class="td">Receiver</span>
		<span class="td">Receiver id</span>
		<span class="td">Destination City</span>
    	<span class="td">Description</span>
    	<span class="td">Tracking</span>
    	
        <span class="td">Operation</span>
    </div>
    <c:forEach var="element" items="${packData}">
     <c:set var="id" value="${element.id}"></c:set>
        <form class="tr" action="UserServlet" method="post">
        	
        	<span class="td">${element.name}</span>
            <span class="td">${element.sender.name}</span>
            <span class="td">${element.senderCity}</span>
            <span class="td">${element.receiver.name}</span>
            <span class="td">${element.receiver.id}</span>
            <span class="td">${element.destinationCity}</span>
            <span class="td">${element.description}</span>
            <span class="td">${element.tracking}</span>
             
                     
            <span class="td">
            	<input type="hidden" value="${element.id}" name="elemId"> 
                <input type="submit" value="Route" name="oEditRoute"> 
                
            </span> 
        </form>
    </c:forEach>
    </div>
	
	<br>
	
	
	</fieldset>
	</details>
	
	
	</td>
	<td width=10--%> </td>
</tr>
</table>
<script>
	var paramOne ="${message}";
	if(paramOne!=null && paramOne!="" && paramOne.length!=0)
		alert(""+paramOne);
</script>
</body>
</html>